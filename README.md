A simple "billing" API, that use **Python**, **Hug** for API and **PeeWee** for ORM.

**User** can create a number of **Wallets**. Each **Wallet** have a **Currency**.
If **Wallet** have a positive balance **User** can transfer money to another **Wallet** his or someone else.

This API uses session based authentication. Most endpoints accessable only by authorized **Users**. 
To use API **User** should be authorized by providing his email and password on `/login` endpoint.

Un-authorized **User** can use next endpoints:
- GET/POST `/login` — Authorise **User** in the system.
- POST `/user` or GET `/user/create` — Create **USER**. _email_ and _password_ should be provided.
- GET `/rates/` — Show current rates between different **Currency** to USD
- GET `/rates/refresh` — Fetch and update current rates.

Authorized **User** can use next endpoints:
- GET `/user/me` — Get information about Authorized **User**.
- POST `/wallet` or GET `/wallet/create` — Create **Wallet**. _currency_symbol_ should be provided.
- DELETE `/wallet` or GET `/wallet` — Delete **Wallet** if balance is 0. _wallet_id_ should be provided. 
- GET/POST `/wallet/deposit` — Deposit funds to a **Wallet**. _wallet_id_ and _amount_ should be provided.
- GET/POST `/wallet/transfer` — Transfer funds between **Wallets**. _source_wallet_id_, _target_wallet_id_ and _amount_ should be provided.
- GET `/report` — Return information about user **Wallets** operations. Output format is json or CSV. _start_ts_, _end_ts_ and 
_output_format_ may be provided. _start_ts_, _end_ts_ is a timestamp, _output_format_ is "json" or "csv".

The one can start _dev_server_ by building python virtual environment, installing dependencies and stating `python dev_server.py`

Most of the code is covered with positive tests.
- Test coverage 92% files, 85% lines.  