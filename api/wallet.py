import hug
from playhouse.shortcuts import model_to_dict

from api.auth import authorized_user
from peewee_models import Wallet


def to_dict(data):
    if isinstance(data, Wallet):
        return model_to_dict(
            data,
            recurse=False,
            backrefs=False,
            exclude={
                Wallet.created_at,
            }
        )


@hug.get('/', requires=authorized_user)
def wallet(user: hug.directives.user):
    """
    Show user wallets
    :return:
    """
    return [to_dict(x) for x in Wallet.select().where(Wallet.user_id == user)]


@hug.post('/', requires=authorized_user)
@hug.get('/create', requires=authorized_user)
def wallet_create(
        user: hug.directives.user,
        currency_symbol: hug.types.text):

    return to_dict(user.create_wallet(currency_symbol))


@hug.delete('/', requires=authorized_user)
@hug.get('/delete', requires=authorized_user)
def wallet_delete(
        user: hug.directives.user,
        wallet_id: hug.types.number
):
    user.delete_wallet(wallet_id)

    return 'Success'


@hug.http('/deposit', accept=['GET', 'POST'], requires=authorized_user)
def wallet_deposit(
        user: hug.directives.user,
        wallet_id: hug.types.number,
        amount: hug.types.number
):
    return to_dict(user.get_wallet(wallet_id).deposit(amount))


@hug.http('/transfer', accept=['GET', 'POST'], requires=authorized_user)
def wallet_transfer(
        user: hug.directives.user,
        source_wallet_id: hug.types.number,
        target_wallet_id: hug.types.number,
        amount: hug.types.number
):
    source_wallet, target_wallet = user.get_wallet(source_wallet_id).transfer(target_wallet_id, amount)
    return to_dict(source_wallet), to_dict(target_wallet)
