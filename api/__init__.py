import hug
from hug.middleware import SessionMiddleware

from api import user, rates, wallet, auth, report


class SessionStore:
    """
    Simple session store, keep it's state only during interpreter run.

    Not suited for production of course.
    """
    store = {}

    def get(self, session_id):
        return self.store[session_id]

    def exists(self, session_id):
        return session_id in self.store

    def set(self, session_id, session_data):
        self.store[session_id] = session_data


session_store = SessionStore()

api = hug.API(__name__)

api.http.add_middleware(SessionMiddleware(
    store=session_store,
    cookie_expires=None,
    cookie_max_age=None,
    cookie_domain=None,
    cookie_path='/',
    cookie_secure=False,
    cookie_http_only=True,
))


@hug.exception(Exception)
def handle_exception(exception):
    if isinstance(exception, hug.HTTPError):
        raise
    description = exception.args[1:]
    if len(description) == 1:
        description = description[0]
    raise hug.HTTPError(
        status='500 Internal Server Error',
        title=exception.args[0],
        description=description or None
    )


api.extend(auth, '/login')
api.extend(user, '/user')
api.extend(rates, '/rates')
api.extend(wallet, '/wallet')
api.extend(report, '/report')