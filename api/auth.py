from hashlib import blake2b

import hug
from hug.authentication import authenticator

from peewee_models import User


def blake2hash(s):
    if isinstance(s, bytes):
        return blake2b(s).hexdigest()
    if isinstance(s, str):
        return blake2b(s.encode()).hexdigest()
    raise ValueError('Input should be string or bytes')


@authenticator
def session_authentication(request, response, verify_user, context=None, **kwargs):
    """Authorized User"""
    user_id = request.context['session'].get('user_id')
    user = User.get_or_none(User.id == user_id)
    if user:
        return user
    return None


@hug.http('/', accept=['GET', 'POST'])
def authenticate(
        session: hug.directives.session,
        email: hug.types.text,
        password: hug.types.text = None,
        password_hash: hug.types.text = None,
):
    password_hash = password_hash or blake2hash(password)

    user = User.get_or_none(User.email == email, User.password_hash == password_hash)

    if user:
        session['user_id'] = user.id
    else:
        raise LookupError('Email or Password is incorrect')
    return {}


authorized_user = session_authentication(None)
