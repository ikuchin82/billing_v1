import hug
from email_validator import validate_email
from playhouse.shortcuts import model_to_dict

from api.auth import blake2hash, authorized_user
from peewee_models import User


@hug.get('/me', requires=authorized_user)
def user_get(
        user: hug.directives.user
):
    return model_to_dict(
        user,
        recurse=False,
        backrefs=False,
        exclude={
            User.password_hash,
            User.created_at,
        }
    )


@hug.post('/', status='201 Created')
@hug.get('/create', status='201 Created')
def user_create(
        email: hug.types.text,
        password: hug.types.text = None,
        password_hash: hug.types.text = None,
        **kwargs
):
    validate_email(
        email,
        check_deliverability=False  # Don't wanna mock this functionality in unit tests
    )

    if not any([password, password_hash]):
        raise AssertionError("password or password_hash should be provided")

    password_hash = password_hash or blake2hash(password)

    record = User.create(
        email=email,
        password_hash=password_hash,
        **kwargs
    )

    return model_to_dict(
        record,
        recurse=False,
        backrefs=False,
        exclude={
            User.password_hash,
            User.created_at,
        })
