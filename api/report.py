import csv
from datetime import datetime
from io import StringIO

import hug
from hug.output_format import json

from api.auth import authorized_user
from peewee_models import Wallet, WalletEvent, User, Currency


@hug.get('/', requires=authorized_user)
def report(
        user: hug.directives.user,
        start_ts: hug.types.number = None,
        end_ts: hug.types.number = None,
        output_format: hug.types.one_of(['json', 'csv']) = None,
        response=None,
):
    """
    Return wallet transaction, support json and csv formats.

    Pagination not supported
    :return:
    """
    output_format = output_format or 'json'

    start_ts = datetime.fromtimestamp(start_ts) if start_ts else datetime.fromtimestamp(0)
    end_ts = datetime.fromtimestamp(end_ts) if end_ts else datetime.now()

    data = list(
        WalletEvent.select(
            WalletEvent.id,
            Wallet.id.alias('wallet_id'),
            User.id.alias('wallet_user_id'),
            WalletEvent.created_at,
            WalletEvent.operation,
            WalletEvent.amount,
            WalletEvent.related_event,
            Currency.symbol
        ).join(
            Wallet
        ).join(
            Currency
        ).join(
            User, on=User.id == Wallet.user_id
        ).where(
            WalletEvent.created_at >= start_ts,
            WalletEvent.created_at <= end_ts,
            WalletEvent.actor_id == user
        ).dicts()
    )

    if output_format == 'json':
        return data
    elif output_format == 'csv':
        f = StringIO()
        if data:
            csv_writer = csv.DictWriter(f, fieldnames=data[0].keys())
            csv_writer.writeheader()
            csv_writer.writerows(data)
            f.seek(0)

        if response:
            response.content_type = 'file/csv; charset=utf-8'
            response.set_header("Content-Disposition", "attachment;filename=report.csv")

        return f.read()
    else:
        raise AssertionError(f'Unknown output format {format}')
