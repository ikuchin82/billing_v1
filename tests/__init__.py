import json
from unittest import TestCase

import hug
from playhouse.shortcuts import dict_to_model

import api
import peewee_models
from api.auth import blake2hash
from settings import pwd


def auth_cookie(email, password):
    response = hug.test.get(api_or_module=api, url='/login', params={
        'email': email, 'password': password})

    return response.headers_dict['set-cookie']


def load_currency():
    with open(f'{pwd}/tests/rates.json') as f:
        peewee_models.Currency.bulk_create([
            dict_to_model(peewee_models.Currency, x)
            for x in json.loads(f.read())
        ])


class BaseTest(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        peewee_models.db.create_tables([
            peewee_models.User,
            peewee_models.Currency,
            peewee_models.Wallet,
            peewee_models.WalletEvent,
        ])

        load_currency()

        cls.password = 'password'
        cls.password_hash = blake2hash(cls.password)

        cls.user = {
            'email': 'test_1@mail.com',
            'first_name': 'first_name',
            'last_name': 'last_name',
            'city': 'NewYork',
            'country': 'US'
        }

        peewee_models.User.create(password_hash=cls.password_hash, **cls.user)

        cls.auth_cookie = auth_cookie(cls.user['email'], cls.password)

    @classmethod
    def tearDownClass(cls) -> None:
        peewee_models.db.drop_tables([
            peewee_models.User,
            peewee_models.Currency,
            peewee_models.Wallet,
            peewee_models.WalletEvent,
        ])

    def setUp(self) -> None:
        peewee_models.db.session_start()

    def tearDown(self) -> None:
        peewee_models.db.session_rollback()

    # Helper function
    def create_wallet(self, currency_symbol):
        response = hug.test.post(
            api_or_module=api,
            url='/wallet',
            params={
                'currency_symbol': currency_symbol
            },
            headers={'cookie': self.auth_cookie}
        )
        self.assertEqual('200 OK', response.status)
        return response.data

    def deposit_funds(self, wallet_id, amount):
        response = hug.test.post(
            api_or_module=api,
            url='/wallet/deposit',
            params={
                'wallet_id': wallet_id,
                'amount': amount
            },
            headers={'cookie': self.auth_cookie}
        )
        self.assertEqual('200 OK', response.status)
        return response.data

    def transfer_funds(self, source_wallet_id, target_wallet_id, amount):
        response = hug.test.post(
            api_or_module=api,
            url='/wallet/transfer',
            params={
                'source_wallet_id': source_wallet_id,
                'target_wallet_id': target_wallet_id,
                'amount': amount
            },
            headers={'cookie': self.auth_cookie}
        )
        self.assertEqual('200 OK', response.status)
        return response.data
