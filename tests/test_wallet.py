import hug

import api
from tests import BaseTest


class TestApiWallet(BaseTest):
    def test_wallet_create(self):
        self.assertEqual(
            {'id': 1, 'currency_id': 27, 'user_id': 1, 'balance': 0},
            self.create_wallet('USD')
        )

        self.assertEqual(
            {'id': 2, 'currency_id': 14, 'user_id': 1, 'balance': 0},
            self.create_wallet('RUB')
        )

    def test_wallet_create_with_invalid_currency(self):
        response = hug.test.post(
            api_or_module=api,
            url='/wallet',
            params={
                'currency_symbol': 'zzz'
            },
            headers={'cookie': self.auth_cookie}
        )
        self.assertEqual('500 Internal Server Error', response.status)
        self.assertEqual(
            {'errors': {'Unknown zzz currency symbol': None}},
            response.data
        )

    def test_wallet_invalid_transaction_not_enough_funds(self):
        usd_wallet_id = self.create_wallet('USD')['id']
        rub_wallet_id = self.create_wallet('RUB')['id']

        response = hug.test.post(
            api_or_module=api,
            url='/wallet/transfer',
            params={
                'source_wallet_id': usd_wallet_id,
                'target_wallet_id': rub_wallet_id,
                'amount': 100
            },
            headers={'cookie': self.auth_cookie}
        )
        self.assertEqual('500 Internal Server Error', response.status)
        self.assertEqual(
            {'errors': {"Source wallet don't have enough funds": {'balance': 0, 'amount': 100}}},
            response.data
        )

    def test_wallet_valid_transaction(self):
        usd_wallet_id = self.create_wallet('USD')['id']
        rub_wallet_id = self.create_wallet('RUB')['id']

        self.deposit_funds(usd_wallet_id, 200)

        self.assertEqual(
            [
                {'id': 1, 'currency_id': 27, 'user_id': 1, 'balance': 100},
                {'id': 2, 'currency_id': 14, 'user_id': 1, 'balance': 6369}
            ],
            self.transfer_funds(usd_wallet_id, rub_wallet_id, 100)
        )
