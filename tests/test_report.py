import json

import hug

import api
from settings import pwd
from tests import BaseTest


class TestApiWallet(BaseTest):
    @staticmethod
    def drop_created_at(data):
        """
        I was lazy to mock datetime in tests, so I'm just dropping it

        :param data:
        :return:
        """
        for x in data:
            x.pop('created_at')

        return data

    def test_transactions_between_own_wallets(self):
        """
        :return:
        """

        usd_wallet_id = self.create_wallet('USD')['id']
        rub_wallet_id = self.create_wallet('RUB')['id']

        self.deposit_funds(usd_wallet_id, 200)
        self.transfer_funds(usd_wallet_id, rub_wallet_id, 10)
        self.transfer_funds(usd_wallet_id, rub_wallet_id, 20)
        self.transfer_funds(usd_wallet_id, rub_wallet_id, 30)
        self.deposit_funds(rub_wallet_id, 100)
        self.transfer_funds(rub_wallet_id, usd_wallet_id, 10)
        self.transfer_funds(rub_wallet_id, usd_wallet_id, 20)
        self.transfer_funds(rub_wallet_id, usd_wallet_id, 30)

        response = hug.test.get(
            api_or_module=api,
            url='/report',
            # params={
            #     'currency_symbol': currency_symbol
            # },
            headers={'cookie': self.auth_cookie}
        )
        self.assertEqual('200 OK', response.status)

        with open(f'{pwd}/tests/sample_report.json') as f:
            sample_report = self.drop_created_at(json.load(f))

        # This tests makes 10 operations:
        # 2 operations "creating wallets"
        # 2 operations "depositing funds"
        # 6 operations "transferring funds"
        # But each "transferring funds" operations creating 2 WalletEvents - "deduction" and "depositing"
        # So  final report will have 2+2+6*2 = 16 operations tottal
        self.assertEqual(
            16,
            len(response.data)
        )

        self.assertEqual(
            sample_report,
            self.drop_created_at(response.data)
        )

