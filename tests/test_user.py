import hug

import api
from tests import BaseTest


class TestApiUser(BaseTest):
    def test_user_create(self):
        new_user = dict(self.user, email='test_2@mai.com')
        params = dict(new_user, password=self.password)

        response = hug.test.post(api_or_module=api, url='/user', params=params)

        self.assertEqual('201 Created', response.status)
        self.assertEqual(
            first=dict(id=2, **new_user),
            second=response.data
        )

    def test_user_exist(self):
        params = dict(self.user, password=self.password)

        response = hug.test.post(api_or_module=api, url='/user', params=params)
        self.assertEqual('500 Internal Server Error', response.status)
        self.assertEqual(
            first={'errors': {'UNIQUE constraint failed: user.email': None}},
            second=response.data
        )

    def test_user_invalid_email(self):
        new_user = dict(self.user, email='invalid_email')
        response = hug.test.post(api_or_module=api, url='/user', params=new_user)
        self.assertEqual('500 Internal Server Error', response.status)
        self.assertEqual(
            first={'errors': {'The email address is not valid. It must have exactly one @-sign.': None}},
            second=response.data
        )

    def test_user_get(self):
        response = hug.test.get(api_or_module=api, url='/user/me', headers={'cookie': self.auth_cookie})
        self.assertEqual('200 OK', response.status)
        self.assertEqual(
            first=dict(id=1, **self.user),
            second=response.data
        )
