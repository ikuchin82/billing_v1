FROM python:3.7-alpine

WORKDIR /src

COPY . .

RUN pip install -r requirements.txt

EXPOSE 8000/tcp

CMD ["python", "dev_server.py"]