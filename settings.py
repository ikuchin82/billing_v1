import os

from dotenv import load_dotenv

load_dotenv()

environment = os.getenv("ENVIRONMENT", 'test')
pwd = os.path.dirname(__file__)
