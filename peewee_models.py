from datetime import datetime

from settings import environment, pwd

from peewee import Model, CharField, DateField, SqliteDatabase, ForeignKeyField, IntegerField, DateTimeField, FloatField

if environment == 'test':
    sqlite_db = ':memory:'
else:
    sqlite_db = f'{pwd}/sqlite.db'

print(f'SQLiteDB: {sqlite_db}')
db = SqliteDatabase(sqlite_db)


class BaseModel(Model):
    created_at = DateTimeField(default=datetime.now)

    class Meta:
        database = db  # This model uses the "people.db" database.


class User(BaseModel):
    first_name = CharField(null=True)
    last_name = CharField(null=True)
    email = CharField(unique=True)
    password_hash = CharField()
    country = CharField(null=True)
    city = CharField(null=True)

    def get_wallet(self, wallet_id):
        return Wallet.get(Wallet.id == wallet_id, Wallet.user_id == self.id)

    def create_wallet(self, currency_symbol: str):
        currency = Currency.get_or_none(Currency.symbol == currency_symbol.upper())

        if currency:
            wallet = Wallet.create(
                currency_id=currency,
                user_id=self.id,
                balance=0
            )

            WalletEvent.create(wallet_id=wallet.id, actor_id=self.id, operation='created')

            return wallet
        else:
            raise LookupError(f'Unknown {currency_symbol} currency symbol')

    def delete_wallet(self, wallet_id: int):
        wallet = Wallet.get(
            Wallet.id == wallet_id,
            Wallet.user_id == self.id,
        )

        if wallet.balance == 0:
            Wallet.delete_by_id(wallet_id)
            WalletEvent.create(wallet_id=wallet_id, actor_id=self.id, operation='deleted')
        else:
            raise AssertionError(f"Can't delete the wallet with balance not equal ot 0", {'balance': wallet.balance})


class Currency(BaseModel):
    symbol = CharField(unique=True)
    last_update = DateField()
    name = CharField()
    rate = FloatField()  # I could use DecimalField but decide to stick to FloatField


class Wallet(BaseModel):
    currency_id = ForeignKeyField(Currency)
    user_id = ForeignKeyField(User)
    balance = IntegerField()  # This value will be in cents

    def deposit(self, amount):
        if not isinstance(amount, int) or amount < 0:
            raise AssertionError('Can deposit only whole positive amount', {'amount': amount})

        self.balance += amount
        self.save()
        WalletEvent.create(
            wallet_id=self.id, actor_id=self.user_id,
            operation='deposit', amount=amount)
        return self

    def transfer(self, target_wallet_id: int, amount: int):
        """
        Transfer possible only if balance of current wallet is grater or equal to requested amount

        :param target_wallet_id:
        :param wallet_id:
        :param amount:
        :return:
        """
        self.get(Wallet.id == target_wallet_id)  # Make sure target wallet exist

        with db.atomic():
            rate_query = Wallet.select(Wallet.id, Currency.rate).join(Currency).where(Wallet.id << (self.id, target_wallet_id))
            rate_dict = {x.id: x.currency_id.rate for x in rate_query}
            target_amount = amount / rate_dict[self.id] * rate_dict[target_wallet_id]

            deducted = Wallet.update(
                balance=Wallet.balance - amount
            ).where(
                Wallet.id == self.id, Wallet.balance >= amount
            ).execute()
            if deducted == 0:
                raise AssertionError("Source wallet don't have enough funds", {'balance': self.balance, 'amount': amount})
            deduction_event = WalletEvent.create(
                wallet_id=self.id, actor_id=self.user_id,
                operation='deduction', amount=amount)

            deposit = Wallet.update(
                balance=Wallet.balance + target_amount
            ).where(
                Wallet.id == target_wallet_id
            ).execute()

            if deposit == 0:
                raise AssertionError("Funds wasn't transferred to target wallet")

            deposit_event = WalletEvent.create(
                wallet_id=target_wallet_id, actor_id=self.user_id,
                operation='deposit', amount=target_amount, related_event=deduction_event.id)

            # I want to have all events linked, right now only 2 events can be linked, not more.
            deduction_event.related_event = deposit_event.id
            deduction_event.save()

        return self.get(Wallet.id == self.id), self.get(Wallet.id == target_wallet_id)


class WalletEvent(BaseModel):
    wallet_id = ForeignKeyField(Wallet)
    actor_id = ForeignKeyField(User)
    operation = CharField()
    amount = IntegerField(null=True)
    related_event = IntegerField(null=True)
