from hug import development_runner
import peewee_models
from api import user, rates


def reset():
    peewee_models.db.drop_tables([
        peewee_models.Currency,
        peewee_models.User,
        peewee_models.Wallet,
        peewee_models.WalletEvent,
    ])


def init():
    peewee_models.db.create_tables([
        peewee_models.Currency,
        peewee_models.User,
        peewee_models.Wallet,
        peewee_models.WalletEvent,
    ])

    try:
        user.user_create(email='test_1@mail.com', password='password')
    except:
        pass
    rates.refresh()


if __name__ == "__main__":
    init()
    development_runner.hug('api/__init__.py')
